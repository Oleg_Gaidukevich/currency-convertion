import { shallow } from "enzyme";
import CurrencyTable from "../CurrencyTable";
import { mockConversionData } from "../../../mock";

describe("CurrencyTable", () => {
  const props = {};

  describe("CurrencyTable initial", () => {
    it("shallow renders", () => {
      shallow(<CurrencyTable {...props} />);
    });

    let CurrencyTableWrapper;

    beforeEach(() => {
      CurrencyTableWrapper = shallow(<CurrencyTable {...props} />);
    });

    it("CurrencyTable snapshot match", () => {
      expect(CurrencyTableWrapper).toMatchSnapshot();
    });

    it("render component DOM", () => {
      expect(
        CurrencyTableWrapper.find("table.currency-table__table")
      ).toHaveLength(1);
      expect(CurrencyTableWrapper.find("th")).toHaveLength(3);
      expect(CurrencyTableWrapper.find("tr")).toHaveLength(2);
      expect(
        CurrencyTableWrapper.find("td.currency-table__td-max-height")
      ).toHaveLength(1);
    });

    it("render component DOM with data", () => {
      CurrencyTableWrapper.setProps({
        data: mockConversionData,
      });
      expect(CurrencyTableWrapper.find("tr")).toHaveLength(
        1 + mockConversionData.length
      );
      expect(
        CurrencyTableWrapper.find("td.currency-table__td-max-height")
      ).toHaveLength(0);
    });
  });
});
