import "./currency-table.scss";

const getChangingTrend = (list, index) =>
  (list[index].value - (list[index - 1]?.value || 0)).toFixed(6);

/**
 * Component that displays a table with data on currency conversion,
 * includes three columns: date, currency conversion and trend.
 *
 * @component
 * @param { data } data Array of objects containing the date and conversion value
 * @example
 * const conversionsList = [{date: 12345, value: 12345}]
 * return (
 *   <CurrencyTable data={conversionsList}/>
 * )
 */

const CurrencyTable = ({ data }) => (
  <div className="currency-table">
    <table className="currency-table__table">
      <thead>
        <tr>
          <th>Date</th>
          <th>Rate</th>
          <th>Changing Trend</th>
        </tr>
      </thead>
      <tbody>
        {data ? (
          data.map(({ date, value }, index, arr) => {
            const changingTrend = index
              ? getChangingTrend(arr, index)
              : "0.000000";
            const isPositiveTrend = changingTrend >= 0;

            return (
              <tr key={date}>
                <td className="currency-table__td">{date}</td>
                <td className="currency-table__td">{value}</td>
                <td
                  className={`currency-table__td currency-table__td-${
                    isPositiveTrend ? "green" : "red"
                  }`}
                >
                  {isPositiveTrend && changingTrend ? "+" : ""}
                  {changingTrend}
                </td>
              </tr>
            );
          })
        ) : (
          <tr>
            <td
              colSpan={3}
              className="currency-table__td currency-table__td-max-height"
            >
              Select currencies
            </td>
          </tr>
        )}
      </tbody>
    </table>
  </div>
);

export default CurrencyTable;
