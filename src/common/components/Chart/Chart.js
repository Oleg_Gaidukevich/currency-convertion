import { VictoryChart, VictoryLine, VictoryTheme } from "victory";
import "./chart.scss";

/**
 * A function that returns a day and month in the "dd.mm" format
 *
 * @function
 * @property {Number} date Date in millisecond
 * @return {String} in the "dd.mm" format
 */

const getFormatDate = (date) =>
  new Date(date).toLocaleString("ru", {
    month: "numeric",
    day: "numeric",
    timezone: "UTC",
  });

/**
 * A component that displays a graph of changes in a trend graph
 *
 * @component
 * @param { data } data Array of objects containing the date and conversion value
 * @example
 * const conversionsList = [{date: 12345, value: 12345}]
 * return (
 *   <Chart data={conversionsList}/>
 * )
 */

const Chart = ({ data }) => (
  <div className="chart">
    <VictoryChart theme={VictoryTheme.material}>
      <VictoryLine
        style={{
          data: { stroke: "#c43a31" },
          parent: { border: "1px solid #ccc" },
        }}
        data={data?.map(({ date, value: y }) => ({
          x: getFormatDate(date),
          y,
        }))}
      />
    </VictoryChart>
  </div>
);

export default Chart;
