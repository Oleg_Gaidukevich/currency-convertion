import { shallow } from "enzyme";
import Chart from "../Chart";
import { mockConversionData } from "../../../mock";

describe("Chart", () => {
  const props = {};

  describe("Chart initial", () => {
    it("shallow renders", () => {
      shallow(<Chart {...props} />);
    });

    let ChartWrapper;

    beforeEach(() => {
      ChartWrapper = shallow(<Chart {...props} />);
    });

    it("Chart snapshot match", () => {
      expect(ChartWrapper).toMatchSnapshot();
    });

    it("render component DOM", () => {
      expect(ChartWrapper.find("VictoryChart")).toHaveLength(1);
    });

    it("render component DOM with data", () => {
      ChartWrapper.setProps({
        data: mockConversionData,
      });
      expect(ChartWrapper.find("VictoryChart")).toHaveLength(1);
    });
  });
});
