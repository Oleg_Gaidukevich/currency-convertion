export const mockConversionData = [
  { date: 123456, value: 123456 },
  { date: 123456, value: 123456 },
  { date: 123456, value: 123456 },
];

export const mockCurrenciesRequest = {
  data: {
    results: {
      ALL: {
        currencyName: "Albanian Lek",
        currencySymbol: "Lek",
        id: "ALL",
      },
      XCD: {
        currencyName: "East Caribbean Dollar",
        currencySymbol: "$",
        id: "XCD",
      },
      EUR: {
        currencyName: "Euro",
        currencySymbol: "€",
        id: "EUR",
      },
      BBD: {
        currencyName: "Barbadian Dollar",
        currencySymbol: "$",
        id: "BBD",
      },
      BTN: {
        currencyName: "Bhutanese Ngultrum",
        id: "BTN",
      },
    },
  },
  status: 200,
};

export const mockCurrenciesVm = [
  { label: "Albanian Lek", symbol: "Lek", value: "ALL" },
  { label: "East Caribbean Dollar", symbol: "$", value: "XCD" },
  { label: "Euro", symbol: "€", value: "EUR" },
  { label: "Barbadian Dollar", symbol: "$", value: "BBD" },
  { label: "Bhutanese Ngultrum", symbol: undefined, value: "BTN" },
];

export const mockConversionResponse = {
  data: {
    ALL_BND: {
      "2021-02-17": 0.012922,
      "2021-02-18": 0.012969,
      "2021-02-19": 0.012975,
      "2021-02-20": 0.01299,
      "2021-02-21": 0.01299,
      "2021-02-22": 0.013002,
      "2021-02-23": 0.012991,
      "2021-02-24": 0.012981,
    },
  },
};

export const mockConversionAllBndVm = {
  ALL_BND: [
    { date: "2021-02-17", value: 0.012922 },
    { date: "2021-02-18", value: 0.012969 },
    { date: "2021-02-19", value: 0.012975 },
    { date: "2021-02-20", value: 0.01299 },
    { date: "2021-02-21", value: 0.01299 },
    { date: "2021-02-22", value: 0.013002 },
    { date: "2021-02-23", value: 0.012991 },
    { date: "2021-02-24", value: 0.012981 },
  ],
};
