import { reducers } from "../../index";
import {
  extraCurrenciesConvert,
  extraGetCurrencies,
  getCurrencies,
  getCurrenciesConvert,
} from "../../../thunk";
import { currencyReducer } from "../currencyReducer";
import {
  mockConversionResponse,
  mockCurrenciesRequest,
  mockCurrenciesVm,
  mockConversionAllBndVm,
} from "../../../common/mock";
import { currencyConvertReducer } from "../dataReducer";

const INITIAL_STATE = { items: {}, data: { conversions: {} } };

describe("reducers", () => {
  it("should return the initial state", () => {
    const state = reducers(undefined, {});

    expect(state).toEqual(INITIAL_STATE);
  });

  it("should handle getCurrencies", () => {
    const state = currencyReducer(undefined, {});

    currencyReducer(state, {
      reducer: extraGetCurrencies[getCurrencies.fulfilled](state, {
        payload: mockCurrenciesRequest,
      }),
      action: { type: "getCurrencies/fulfilled" },
    });

    expect(state.currencies).toEqual(mockCurrenciesVm);
  });

  it("should handle getCurrenciesConvert", () => {
    const state = currencyConvertReducer(undefined, {});

    currencyConvertReducer(state, {
      reducer: extraCurrenciesConvert[getCurrenciesConvert.fulfilled](state, {
        meta: { arg: "ALL_BND" },
        payload: mockConversionResponse,
      }),
      action: { type: "getCurrenciesConvert/fulfilled" },
    });

    expect(state.conversions).toEqual(mockConversionAllBndVm);
  });
});
