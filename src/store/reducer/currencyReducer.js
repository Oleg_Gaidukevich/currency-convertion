import { createSlice } from "@reduxjs/toolkit";
import { extraGetCurrencies } from "../../thunk";

/**
 * Object that stores props for working with redux: reducers, storage, actions.
 * This object works with currency data.
 * Contains the processing of an asynchronous request to the server
 * for a list of currencies.
 */

const currencySlice = createSlice({
  name: "items",
  initialState: {},
  extraReducers: {
    ...extraGetCurrencies,
  },
});

export const currencyReducer = currencySlice.reducer;
