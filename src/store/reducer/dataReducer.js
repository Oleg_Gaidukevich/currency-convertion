import { createSlice } from "@reduxjs/toolkit";
import { extraCurrenciesConvert } from "../../thunk";

/**
 * Object that stores props for working with redux: reducers, storage, actions.
 * This object works with currency conversion data.
 * Contains the processing of an asynchronous request to the server
 * for a list of currency conversions.
 *
 */

const currencyConvertSlice = createSlice({
  name: "data",
  initialState: { conversions: {} },
  extraReducers: {
    ...extraCurrenciesConvert,
  },
});

export const currencyConvertReducer = currencyConvertSlice.reducer;
