import {
  configureStore,
  getDefaultMiddleware,
  combineReducers,
} from "@reduxjs/toolkit";
import { persistReducer, persistStore } from "redux-persist";
import { currencyReducer } from "./reducer/currencyReducer";
import storage from "redux-persist/lib/storage";
import { currencyConvertReducer } from "./reducer/dataReducer";

const persistConfig = {
  key: "conversion",
  storage,
  whitelist: ["items", "data"],
};

export const reducers = combineReducers({
  items: currencyReducer,
  data: currencyConvertReducer,
});

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: false,
  }),
});

export const persistor = persistStore(store);
