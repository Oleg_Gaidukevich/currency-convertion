import { createAsyncThunk } from "@reduxjs/toolkit";
import { fetchCurrencies, fetchCurrenciesConvert } from "../api";
import {
  transformCurrenciesToVm,
  transformCurrenciesConvertToVm,
} from "./transformers";

/**
 * Function for an asynchronous request to the server to get a list of currencies,
 * bound to the action and parameters for working with redux.
 */

export const getCurrencies = createAsyncThunk("getCurrencies", fetchCurrencies);

/**
 * Set of functions for asynchronous processing of data from the server
 */

export const extraGetCurrencies = {
  [getCurrencies.pending]: (state, action) => {},
  [getCurrencies.fulfilled]: (state, { payload }) => {
    state.currencies = transformCurrenciesToVm(payload);
  },
  [getCurrencies.rejected]: (state, action) => {},
};

/**
 * Function for an asynchronous request to the server to get a list of currency converse,
 * with binding to an action and parameters for working with redux
 */

export const getCurrenciesConvert = createAsyncThunk(
  "getCurrenciesConvert",
  fetchCurrenciesConvert
);

/**
 * Set of functions for asynchronous processing of data from the server
 */

export const extraCurrenciesConvert = {
  [getCurrenciesConvert.pending]: (state, action) => {},
  [getCurrenciesConvert.fulfilled]: (state, { meta: { arg }, payload }) => {
    state.conversions[arg] = transformCurrenciesConvertToVm(payload, arg);
  },
  [getCurrenciesConvert.rejected]: (state, action) => {},
};
