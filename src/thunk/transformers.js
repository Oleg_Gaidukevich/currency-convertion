/**
 * Function for transforming the list of currencies coming
 *  from the server into data for easy use in the application
 *
 * @function
 *  @param   {Object}  serverData an object with an array of
 *                                currencies coming from REST API
 *
 * @return {Array} array with objects containing data about currencies
 */

export const transformCurrenciesToVm = ({ data: { results } }) =>
  Object.values(
    Object.values(results).map(({ id, currencyName, currencySymbol }) => ({
      value: id,
      label: currencyName,
      symbol: currencySymbol,
    }))
  );

/**
 * The function of converting the list of currency conversions received
 *  from the server into data for easy use in the application
 *
 * @function
 *  @param   {Object}  serverData an object with an array of
 *                                currency conversions coming from REST API
 *
 * @return {Array} array with objects containing data about currency conversions
 */

export const transformCurrenciesConvertToVm = ({ data }) =>
  Object.values(data)
    .map((values) =>
      Object.entries(values).map(([date, value]) => ({ date, value }))
    )
    .pop();
