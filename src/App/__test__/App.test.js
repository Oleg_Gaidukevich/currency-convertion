import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { shallow } from "enzyme";

import App from "../App";

describe("App", () => {
  const initialState = { items: { currencies: [] }, data: { conversions: {} } };
  const mockStore = configureStore();
  const store = mockStore(initialState);

  describe("App initial", () => {
    it("shallow renders", () => {
      shallow(
        <Provider store={store}>
          <App />
        </Provider>
      );
    });

    let AppWrapper;
    beforeEach(() => {
      AppWrapper = shallow(
        <Provider store={store}>
          <App />
        </Provider>
      );
    });

    it("App snapshot match", () => {
      expect(AppWrapper).toMatchSnapshot();
    });
  });
});
