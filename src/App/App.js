import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import CurrencyPage from "../pages/CurrencyPage";
import { getCurrencies } from "../thunk";

import "./app.scss";

/**
 * Main component for displaying application components
 *
 * @component
 * @example
 * return (
 *   <App />
 * )
 */

const App = () => {
  const dispatch = useDispatch();
  const currencies = useSelector(({ items: { currencies } }) => currencies);

  useEffect(() => {
    if (!currencies) {
      dispatch(getCurrencies());
    }
  }, []);

  return (
    <div className="app">
      <CurrencyPage />
    </div>
  );
};

export default App;
