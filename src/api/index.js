import axios from "axios";
import { FREE_CURRCONV_API_KEY } from "../common/constants";

const api = axios.create({
  baseURL: "https://free.currconv.com/api/v7",
});

/**
 * Function returning list of currencies from the REST API.
 *
 *@function
 * @return {Promise<Array<REST~Currencies>>}
 */

export const fetchCurrencies = () =>
  api.get(`/currencies?apiKey=${FREE_CURRCONV_API_KEY}`);

/**
 * Function returning currencies conversion from the REST API.
 *
 * @function
 *  @param   {String}  compareParam a string consisting ofIDs of two
 *                                  currencies connected by an underscore
 *
 * @return {Promise<Array<REST~CurrenciesConversion>>}
 */

export const fetchCurrenciesConvert = (compareParam) => {
  const dateNow = new Date();
  const currentDate = dateNow.toISOString().slice(0, 10);
  const weekLessDate = new Date(dateNow.setDate(dateNow.getDate() - 7))
    .toISOString()
    .slice(0, 10);

  return api.get(
    `/convert?apiKey=${FREE_CURRCONV_API_KEY}&q=${compareParam}&compact=ultra&date=${weekLessDate}&endDate=${currentDate}`
  );
};
