import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { shallow } from "enzyme";

import CurrencyPage from "../CurrencyPage";

describe("CurrencyPage", () => {
  describe("CurrencyPage initial", () => {
    const initialState = {
      items: { currencies: [] },
      data: { conversions: {} },
    };
    const mockStore = configureStore();
    const store = mockStore(initialState);

    it("shallow renders", () => {
      shallow(
        <Provider store={store}>
          <CurrencyPage />
        </Provider>
      );
    });

    let CurrencyPageWrapper;
    beforeEach(() => {
      CurrencyPageWrapper = shallow(
        <Provider store={store}>
          <CurrencyPage />
        </Provider>
      );
    });

    it("CurrencyPage snapshot match", () => {
      expect(CurrencyPageWrapper).toMatchSnapshot();
    });
  });
});
