import { useEffect, useState, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import Select from "react-select";
import Chart from "../../common/components/Chart";
import CurrencyTable from "../../common/components/CurrencyTable";
import { getCurrenciesConvert } from "../../thunk";

import "./currency-page.scss";

/**
 * Main page that displays: drop-down lists, a table with data and a graph of changes
 *
 * @component
 * @example
 * return (
 *   <CurrencyPage />
 * )
 */

const CurrencyPage = () => {
  const [firstCurrency, setFirstCurrency] = useState();
  const [secondCurrency, setSecondCurrency] = useState();
  const dispatch = useDispatch();
  const { currencies, conversions } = useSelector(
    ({ items: { currencies }, data: { conversions } }) => ({
      currencies,
      conversions,
    })
  );

  const conversionParam = useMemo(
    () =>
      firstCurrency && secondCurrency
        ? `${firstCurrency?.value}_${secondCurrency?.value}`
        : "",
    [firstCurrency, secondCurrency]
  );

  const conversionList = useMemo(() => conversions[conversionParam], [
    conversions,
    conversionParam,
  ]);

  useEffect(() => {
    if (conversionParam && !conversionList) {
      dispatch(getCurrenciesConvert(conversionParam));
    }
  }, [conversionList, conversionParam]);

  return (
    <div className="currency-page">
      <div className="currency-page__select-group">
        <Select
          value={firstCurrency}
          onChange={setFirstCurrency}
          options={currencies?.filter(
            ({ value }) => value !== secondCurrency?.value
          )}
        />
        <Select
          value={secondCurrency}
          onChange={setSecondCurrency}
          options={currencies?.filter(
            ({ value }) => value !== firstCurrency?.value
          )}
        />
      </div>
      <div className="currency-page__content">
        <CurrencyTable data={conversionList} />
        <Chart data={conversionList} />
      </div>
    </div>
  );
};

export default CurrencyPage;
